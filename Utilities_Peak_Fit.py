import csv
import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import find_peaks

class Utilities():

    def __init__(self):
        pass

    # A function to convert text files to CSV format
    def convert_to_csv(self, input_name, output_name):
        input_file = open(input_name, 'r')
        output_file = open(output_name, 'w')
        for line in input_file:
            (shifts, intensities) = line.strip().split('\t')
            output_file.write(','.join([shifts, intensities]) + '\n')
        input_file.close()
        output_file.close()

    # A function to read in the data files and skip any comments
    def read_csv(self, filename):
        data_1 = []
        data_2 = []
        with open(filename) as csvfile:
            # Comments are removed (any line that begins with #) and the data is read into lists
            data = csv.DictReader(filter(lambda row: row[0] != '#', csvfile), fieldnames=['1', '2'])
            for row in data:
                data_1.append(float(row['1']))
                data_2.append(float(row['2']))
        return data_1, data_2

    # A function to plot the spectra and their peak locations with labels
    def plot_spectra(self, x, y, peak_indices, title):
        # The plot is formatted
        plt.style.use('seaborn-bright')
        plt.title(title, weight='bold')
        plt.xlabel('Raman Shift / cm$^{-1}$')
        plt.ylabel('Intensity / arbitrary units')
        plt.grid(alpha=0.4)

        # For each peak a vertical line is plotted and labelled to mark it
        for i in range(0, len(peak_indices)):
            plt.vlines(x[peak_indices[i]], min(y), y[peak_indices[i]], color='darkred')
            plt.annotate('Peak: ' + str(x[peak_indices[i]]) + ' cm$^{-1}$', xy=(x[peak_indices[i]], y[peak_indices[i]] + 0.1*max(y)), horizontalalignment='center')

        # The spectra is plotted
        plt.plot(x, y, color='midnightblue')

        # The plot is sized to scale with any spectra and saved to a PNG file
        plt.xlim(xmin=min(x), xmax=max(x))
        plt.ylim(ymin=min(y), ymax=1.3*max(y))
        #plt.savefig('spectra.png', transparent=True)
        plt.show()


    # A function to plot a comparison of the linear fits for the restricted data set vs. all data
    def fit_compare(self, SO_data, Restricted_SO):
        coeff = np.polyfit(Restricted_SO[0], Restricted_SO[1], 1)
        coeff2 = np.polyfit(SO_data[0], SO_data[1], 1)
        plt.title('Linear Fits', weight='bold')
        plt.plot(SO_data[0], (np.array(SO_data[0])*(coeff[0])) + coeff[1], 'midnightblue', label='Restricted Fit')
        plt.plot(SO_data[0], (np.array(SO_data[0])*coeff2[0] )+ coeff2[1], 'darkred', label='Fit Using All Data')
        plt.scatter(SO_data[0], SO_data[1], color='k', marker='x')
        plt.legend()
        plt.xlabel('Hydration State')
        plt.xticks([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])
        plt.ylabel('Raman Shift / cm$^{-1}$')
        plt.savefig('linear.png', transparent=True)
        plt.show()


class Peak_Matching():

    def __init__(self):
        pass

    # A function to find the peak locations in the spectra
    def peak_location(self, y):
        peak_indices, _ = find_peaks(y, height=0.4*max(y), prominence=50, width=0.3)
        return peak_indices

    # A function to select the correct peak region and account for starkeyite and hexahydrite
    def select_peaks(self, x, peak_indices):
        peaks = []
        indices = []
        starkeyite = False
        hexahydrite = False

        # Running through all detected peaks
        for i in range(len(peak_indices)):
            # Sulphate peak region selected
            if 980 < x[peak_indices[i]] < 1050:
                # Sample checked for starkeyite
                if 999 < x[peak_indices[i]] < 1004.5:
                    starkeyite = True
                    indices.append(peak_indices[i])

                # Sample checked for hexahydrite
                elif 990 < x[peak_indices[i]] < 999:
                    hexahydrite = True
                    indices.append(peak_indices[i])

                else:
                    peaks.append(x[peak_indices[i]])
                    indices.append(peak_indices[i])
            else:
                pass
        return peaks, indices, starkeyite, hexahydrite

    # A function to calculate the hydration state using the linear fit and round it to the nearest integer
    def hydration_state(self, peaks, SO_data):
        coeff = np.polyfit(SO_data[0], SO_data[1], 1)                          # Linear Fit
        SO_hydration = np.round((peaks - coeff[1]) / coeff[0])                 # x = (y - c) / m
        SO_hydration = [int(i) for i in SO_hydration]
        return SO_hydration

    # A function to run the program
    def run(self):
        # The Utilities class is used to read an input file
        util = Utilities()
        shifts, intensities = util.read_csv('Olivia_6_2.txt')
        # Peak locations are found
        peak_indices = self.peak_location(intensities)
        # Peaks are filtered to the sulphate range and checked for starkeyite and hexahydrite
        SO_peaks, peak_indices, starkeyrite, hexahydrite = self.select_peaks(shifts, peak_indices)
        # Literature data is read in
        SO_state, SO_shift = util.read_csv('SO_vib_shifts.txt')
        # The literature data is used to calculate hydration states present from the peaks
        hydration = self.hydration_state(SO_peaks, [SO_state, SO_shift])
        # Starkeyrite and hexahydrite are added to the list if present
        if starkeyrite == True:
            hydration.append(4)
        elif hexahydrite == True:
            hydration.append(6)
        # Duplicates are removed and the hydration states outputted
        print('The hydration state(s) were found to be: ' + str(list(set(hydration))))
        # A spectra is plotted with labelled peaks
        util.plot_spectra(shifts, intensities, peak_indices, 'Regolith & Kieserite Mixe - Lab')

P = Peak_Matching()
P.run()